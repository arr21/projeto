
## Comando para construir imagem 
```terminal
docker build -f docker-config\app\Dockerfile -t seila .
```

## Comando para executar a imagem do app
```terminal
docker run -p 9000:8080 -it seila bash
```