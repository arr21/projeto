package com.example.demo.controller;


import java.time.LocalDateTime;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pacote.Anime;
import com.example.demo.util.DateUtil;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("anime")
@Log4j2
//@AllArgsConstructor
@RequiredArgsConstructor
public class AnimeController {

    //@Autowired
    private final DateUtil dateUtil;



    //localhost:8080/anime/list
    @GetMapping(path = "list")
    public List<Anime> list(){
        
        log.info(dateUtil.formatLocalDateTimeToDatabaseStyle(LocalDateTime.now()));
        return List.of(new Anime("dbz"), new Anime("cdz"));
    }
    
}
